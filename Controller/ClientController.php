<?php

namespace Controller;


use Helpers\Curl;
require_once('defines.php');

class ClientController {

    // client api get users list
    public function index() {

        $apiCall = new Curl();
        $response = $apiCall->call(array(), 'GET');

        $this->render('list', $response);
    }

    // client api get users list by search
    public function find() {

        $lastName = $_POST['lastName'];
        $firstName = $_POST['firstName'];
        $action = $_POST['action'];
        $apiCall = new Curl();
        $response = $apiCall->call(['lastName' => $lastName, 'firstName' => $firstName, 'action' => $action], 'POST');

        $this->render('list', $response);
    }

    // client api add user
    public function add() {

        $data = '';
        if ((isset($_POST['action'])) && ($_POST['action'] == 'add')) {

            $lastName = $_POST['lastName'];
            $firstName = $_POST['firstName'];
            $apiCall = new Curl();
            $response = $apiCall->call(['lastName' => $lastName, 'firstName' => $firstName], 'POST');
            $data = "Utilisateur est crée avec succès.";
        }

        $this->render('add', $data);
    }

    // client api edit user
    public function edit() {

        $id = $_GET['id'];
        $data['message'] = "";
        if ((isset($_POST['action'])) && ($_POST['action'] == 'edit')) {
            $lastName = $_POST['lastName'];
            $firstName = $_POST['firstName'];
            $curl = curl_init();

            $apiCall = new Curl();
            $response = $apiCall->call(["id" => $id, "lastName" => $lastName, "firstName" => $firstName], 'PUT');

            $data['message'] = "Utilisateur a été modifié avec succès.";
        }

        $user = $this->read($_GET['id']);
        $data['user'] = $user;

        $this->render('edit', $data);
    }

    // client api read user
    public function read($id) {

        $apiCall = new Curl();
        $response = $apiCall->call(array('id' => $id), 'GET');

        return $response;
    }

    // client api read user
    public function show() {

        $id = $_GET['id'];
        $apiCall = new Curl();
        $data = $apiCall->call(array('id' => $id), 'GET');

        $this->render('show', $data);
    }
    
    // client api delete user
    public function delete() {

        $id = $_GET['id'];
        if ((isset($id))) {

            $apiCall = new Curl();
            $response = $apiCall->call(array('id' => $id), 'DELETE');

            header('Location: ' . BASE_URL);
        }
    }

    // render template
    public function render(string $fichier, $data = null) {

        require_once('views/' . $fichier . '.php');
    }

}
