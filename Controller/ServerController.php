<?php

namespace Controller;

use Model\User;

class ServerController {

    // server api get users list
    public function list() {

        $user = new User();
        $result = $user->list();

        echo json_encode($result);
    }

    // server api search users list 
    public function find($lastName, $firstName) {

        $user = new User();
        $result = $user->find($lastName, $firstName);

        echo json_encode($result);
    }

    // server api read user 
    public function read($id) {

        $user = new User();
        $result = $user->read($id);

        echo json_encode($result);
    }

    // server api update user 
    public function update($id, $firstName, $lastName) {

        $user = new User();
        $result = $user->update($id, $firstName, $lastName);

        echo json_encode($result);
    }

    // server api create user 
    public function create($firstName, $lastName) {

        $user = new User();
        $result = $user->create($firstName, $lastName);

        if ($result) {
            $status = "Utilisateur est crée avec succès.";
        } else {
            $status = "Utilisateur n'est pas crée avec succès";
        }

        echo $status;
    }

    // server api delete user 
    public function delete($id) {

        $user = new User();
        $result = $user->delete($id);

        echo json_encode($result);
    }

}
