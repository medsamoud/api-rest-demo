<?php

require_once 'vendor/autoload.php';

use Helpers\Router;

$routes = new Router();

$routes->run();

?>