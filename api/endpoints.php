<?php

require_once '../vendor/autoload.php';
use Controller\ServerController;


switch ($_SERVER['REQUEST_METHOD']) {

    case "GET":
        $server = new ServerController();
        if (isset($_GET['id'])) {
            $server->read($_GET['id']);
        } else {
            $server->list();
        }

        break;

    case "POST":
        $server = new ServerController();
        if (isset($_REQUEST['action']) && ($_REQUEST['action'] == 'find')) {
            $server->find($_REQUEST['lastName'], $_REQUEST['firstName']);
        } else {
           $server->create($_REQUEST['lastName'], $_REQUEST['firstName']);
        }

        break;

    case "PUT":

        if (isset($_REQUEST['id'])) {
            $server = new ServerController();
            $server->update($_REQUEST['id'], $_REQUEST['firstName'], $_REQUEST['lastName']);
        }

        break;

    case "DELETE":
        if (isset($_REQUEST['id'])) {
            $server = new ServerController();
            $server->delete($_REQUEST['id']);
        }

        break;

    default:
        break;
}
?>