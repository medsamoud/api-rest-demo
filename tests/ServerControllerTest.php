<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Controller\ServerController;

final class ServerControllerTest extends TestCase
{
    public function testList(): void
    {

        $list = ServerController::list();

        echo $list;
        
        $this->addToAssertionCount(1);
    }
    
    public function testFind(): void
    {
        $lastName = 'test';
        $firstName = 'test';

        $result = ServerController::find($lastName, $firstName);

        echo $result;
        
        $this->addToAssertionCount(1);
    }
    
    public function testRead(): void
    {
        $id = 139;

        $info = ServerController::read($id);

        echo $info;
        
        $this->addToAssertionCount(1);
    }
    
    public function testCreate(): void
    {
        $lastName = 'test phpunit';
        $firstName = 'test phpunit';

        $list = ServerController::create($lastName, $firstName);

        echo $list;
        
        $this->addToAssertionCount(1);
    }
    
    public function testUpdate(): void
    {
        $id = 139;
        $lastName = 'test phpunit Update';
        $firstName = 'test phpunit Update';

        $result = ServerController::create($lastName, $firstName);

        echo $result;
        
        $this->addToAssertionCount(1);
    }
    
    public function testDelete(): void
    {
        $id = 139;

        $result = ServerController::delete($id);

        echo $result;
        
        $this->addToAssertionCount(1);
    }

}
