<?php require_once('defines.php'); ?>
<!DOCTYPE html>
<html>
    <head>
        <title>Demo API REST</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/asset/css/bootstrap.min.css" crossorigin="anonymous">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/asset/css/style.css">
        <script src="<?php echo BASE_URL ?>/asset/js/jquery-3.3.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" crossorigin="anonymous"></script>
        <script src="<?php echo BASE_URL ?>/asset/js/bootstrap.min.js"></script>
        <script src="<?php echo BASE_URL ?>/asset/js/script.js"></script>
    </head>
    <body>
        <?php
        /* include 'Controller/ClientController.php';
          $client = new ClientController();

          if ((isset($_POST['action'])) && ($_POST['action'] == 'find')) {
          $users = $client->find($_POST['action'], $_POST['lastName'], $_POST['firstName']);
          } else if ((isset($_GET['action'])) && ($_GET['action'] == 'delete')) {
          $users = $client->delete($_GET['id']);
          header('Location: index.php');
          } else {
          $users = $client->index();
          } */
        ?>
        <div class="container">
            <div class="p-4 p-md-5 mb-4 rounded text-bg-dark">
                <div class="col-md-6 px-0">
                    <img src="asset/images/logo.png" class="logo-img">
                </div>
            </div>
            <form action="find" method="POST">
                <a href="add" class="btn btn-primary m-md-5">Ajouter</a>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Id</th>
                            <th scope="col">Nom</th>
                            <th scope="col">Prénom</th>
                            <th scope="col">Action</th>
                        </tr>
                        <tr>
                            <th scope="col">Id</th>
                            <th scope="col"><input name="firstName" type="text"></th>
                            <th scope="col"><input name="lastName" type="text"></th>
                            <th scope="col">
                                <button name="action" value="find" type="submit" class="btn btn-info">Chercher</button>
                                <a href="<?php echo BASE_URL ?>">Initialiser</a>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ((isset($data)) &&(count($data) > 0)) : ?>
                            <?php foreach ($data as $key => $user): ?>
                                <tr>
                                    <th scope="row"><?php echo $user->id; ?></th>
                                    <td><a href="show/<?php echo $user->id ?>"><?php echo $user->firstName ?></a></td>
                                    <td><a href="show/<?php echo $user->id ?>"><?php echo $user->lastName ?></a></td>
                                    <td>
                                        <a href="edit/<?php echo $user->id ?>" class="btn btn-primary">Modifier</a>
                                        <a href="delete/<?php echo $user->id ?>" class="btn btn-danger" onclick="return confirmer()">Supprimer</a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                                <tr><td colspan="4"><p><center>Pas d'enregistrement</center></p></td></tr>
                        <?php endif; ?>        
                    </tbody>
                </table>
            </form>                
        </div>
    </body>
</html>