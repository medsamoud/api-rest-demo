<!DOCTYPE html>
<html>
    <head>
        <title>Demo API REST</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/asset/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/asset/css/style.css">
        <script src="<?php echo BASE_URL ?>/asset/js/jquery-3.3.1.slim.min.js"></script>
        <script src="<?php echo BASE_URL ?>/asset/js/popper.min.js"></script>
        <script src="<?php echo BASE_URL ?>/asset/js/bootstrap.min.js"></script>
    </head>
    <body>

        <div class="container">
            <div class="p-4 p-md-5 mb-4 rounded text-bg-dark">
                <div class="col-md-6 px-0">
                    <img src="asset/images/logo.png" class="logo-img">
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <p class="text-info">
                        <?php echo $data ?>
                    </p>
                    <form action="" method="POST">
                        <div class="form-group">
                            <label for="lastName">Nom</label>
                            <input name="lastName" type="text" class="form-control" id="lastName" required="required" placeholder="Votre nom">
                        </div>
                        <div class="form-group">
                            <label for="firstName">Prénom</label>
                            <input name="firstName" type="text" class="form-control" id="firstName" required="required" placeholder="Votre prénom">
                        </div>
                        <input name="action" type="hidden" value="add">
                        <button type="submit" class="btn btn-primary">Envoyer</button>
                        <a href="<?php echo BASE_URL ?>">Retour</a>
                    </form>
                </div>
            </div>

        </div>
    </body>
</html>