<?php

namespace Model;

use Model\Database;
use PDO;

class User {

    public function __construct() {
        $this->pdo = Database::connect();
    }

    // List all users
    public function list() {

        $sql = "SELECT * FROM user ORDER BY id DESC";
        try {

            $query = $this->pdo->prepare($sql);
            $query->execute();
            $users = $query->fetchAll();

            return $users;
        } catch (PDOException $e) {

            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    // find users by name
    public function find($lastName, $firstName) {

        $where = ' WHERE 1=1 ';
        try {
            if ($lastName != '') {
                $where .= " and lastName like '" . $lastName . "%' ";
            }

            if ($firstName != '') {
                $where .= " and firstName like '" . $firstName . "%' ";
            }

            $sql = "SELECT * FROM user " . $where . " ORDER BY id DESC";
            $query = $this->pdo->prepare($sql);
            $query->execute();
            $users = $query->fetchAll(PDO::FETCH_ASSOC);

            return $users;
        } catch (PDOException $e) {

            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    // Get one user
    public function read($id) {

        $this->pdo = Database::connect();
        $sql = "SELECT * FROM user where id = '" . $id . "'";
        try {

            $query = $this->pdo->prepare($sql);
            $query->execute();
            $user = $query->fetch(PDO::FETCH_ASSOC);

            return $user;
        } catch (PDOException $e) {

            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    // Update user info
    public function update($id, $firstName, $lastName) {

        $this->pdo = Database::connect();
        $sql = "UPDATE user SET firstName = '" . addslashes($firstName) . "', lastName = '" . addslashes($lastName) . "' where id = '" . $id . "'";

        $status = [];
        try {

            $query = $this->pdo->prepare($sql);
            $result = $query->execute();
            if ($result) {
                $status['message'] = "Data updated";
            } else {
                $status['message'] = "Data is not updated";
            }

            return $status;
        } catch (PDOException $e) {

            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    // Add new user
    public function create($firstName, $lastName) {

        $this->pdo = Database::connect();
        $sql = "INSERT INTO user(`firstName`,`lastName`) VALUES('" . addslashes($firstName) . "', '" . addslashes($lastName) . "')";

        try {

            $query = $this->pdo->prepare($sql);
            $result = $query->execute();

            return $result;
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    // Delete user
    public function delete($id) {

        $this->pdo = Database::connect();
        $sql = "DELETE FROM user where id = '{$id}'";
        $status = [];
        try {

            $query = $this->pdo->prepare($sql);
            $result = $query->execute();
            if ($result) {
                $status['message'] = "Data deleted";
            } else {
                $status['message'] = "Data is not deleted";
            }

            return $status;
        } catch (PDOException $e) {

            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

}
