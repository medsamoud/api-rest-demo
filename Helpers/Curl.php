<?php

namespace Helpers;

require_once('defines.php');

/**
 * Description of Curl
 *
 * @author Mohamed
 */
class Curl {

    // client api endpoints call
    public function call(array $params, $method) {

        $curl = curl_init();
        $endpoint = 'http://' . $_SERVER['HTTP_HOST'] . BASE_URL . '/api/endpoints.php';

        $url = $endpoint . '?' . http_build_query($params);

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return json_decode($response);
    }

}
