<?php

namespace Helpers;

use Controller\ClientController;


class Router {

    public function run() {

        $params = explode('/', $_GET['p']);
        // Si au moins 1 paramètre existe
        // On sauvegarde le 1er paramètre dans $controller en mettant sa 1ère lettre en majuscule
        // On sauvegarde le 2ème paramètre dans $action si il existe, sinon index
        $action = ($params[0] != '') ? $params[0] : 'index';
        $controller = new ClientController();

        $controller->$action();
    }

}
